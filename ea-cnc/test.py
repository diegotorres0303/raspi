#!/usr/bin/env python  

import RPi.GPIO as GPIO
import time
import signal
import atexit


IN1 = 11    # pin11  
IN2 = 12  
IN3 = 13  
IN4 = 15  

# servopin = 12
servopin = 32

def setStep(w1, w2, w3, w4):  
	GPIO.output(IN1, w1)  
	GPIO.output(IN2, w2)  
	GPIO.output(IN3, w3)  
	GPIO.output(IN4, w4)  

def stop():  
	setStep(0, 0, 0, 0)  

def forward(delay, steps):    
	for i in range(0, steps):  
		setStep(1, 0, 0, 0)  
		time.sleep(delay)  
		setStep(0, 1, 0, 0)  
		time.sleep(delay)  
		setStep(0, 0, 1, 0)  
		time.sleep(delay)  
		setStep(0, 0, 0, 1)  
		time.sleep(delay)  

def backward(delay, steps):    
	for i in range(0, steps):  
		setStep(0, 0, 0, 1)  
		time.sleep(delay)  
		setStep(0, 0, 1, 0)  
		time.sleep(delay)  
		setStep(0, 1, 0, 0)  
		time.sleep(delay)  
		setStep(1, 0, 0, 0)  
		time.sleep(delay)  

def setup():  
	GPIO.setwarnings(False)  
	GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location  
	GPIO.setup(IN1, GPIO.OUT)      # Set pin's mode is output  
	GPIO.setup(IN2, GPIO.OUT)  
	GPIO.setup(IN3, GPIO.OUT)  
	GPIO.setup(IN4, GPIO.OUT)  
	
	# GPIO.setmode(GPIO.BCM)
	GPIO.setup(servopin, GPIO.OUT, initial=False)
	print(IN1)  
	print(IN2)  
	print(IN3)  
	print(IN4)  

	
atexit.register(GPIO.cleanup) 

setup()

p = GPIO.PWM(servopin,50)
p.start(0)
print(GPIO.OUT)
print(p)
# time.sleep(2)


# here all the steps i want =D
backward(0.003, 512)  # 512 steps --- 360 angle  
p.ChangeDutyCycle(2.5)
time.sleep(2)
p.ChangeDutyCycle(10)
time.sleep(2)
