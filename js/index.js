const Gpio = require('pigpio').Gpio
const { Stepper, MODES, FORWARD, BACKWARD } = require('wpi-stepper')

const motor = new Gpio(12, { mode: Gpio.OUTPUT })
//const pins = [11, 12,13,15]
const pins = [17, 18, 27, 22]
const stepMotor = new Stepper({ pins, mode: MODES.SINGLE })
//stepMotor.move(400).then(() => console.log('steps done'))

let pulseWidth = 1000
let increment = 100
console.log(motor)

function moveServo(pos, time = 300) {
  return new Promise(resolve =>
    setTimeout(() => {
      motor.servoWrite(pos)
      resolve(true)
    }, time)
  )
}

console.log('max speed', stepMotor.maxRPM)
stepMotor.speed = 50
moveServo(1000)
  .then(() => moveServo(2000))
  .then(() => moveServo(1000, 500))
  .then(() => moveServo(1500, 750))
  /*
stepMotor
  .move(400)
  .then(() => {
    stepMotor.speed = 10
    return Promise.all([stepMotor.move(-200), moveServo(1500)])
  })
  .then(() => {
    stepMotor.speed = 70
    //stepMotor.run(BACKWARD)
    return Promise.all([stepMotor.move(400), moveServo(2000)])
  })
  .then(() => {
    stepMotor.speed = 30
    //stepMotor.run(FORWARD)
    return Promise.all([stepMotor.move(-800), moveServo(1000)])
  })
  /**/
  .catch(console.error)

/*
setInterval(() => {
	//console.log('jjj', increment)

  motor.servoWrite(pulseWidth)

  pulseWidth += increment
  if (pulseWidth >= 2000) {
    increment = -100
  } else if (pulseWidth <= 1000) {
    increment = 100
  }
}, 1000)/**/
